# Erstellen eines privaten Dev-Servers mit Internetanbindung

Ausführen:
```sh
ssh-keygen -m pem -t rsa -f ~/.ssh/aws_instance_key
export TF_VAR_pubkey=$(cat ~/.ssh/aws_instance_key.pub)
terraform plan
terraform apply
```