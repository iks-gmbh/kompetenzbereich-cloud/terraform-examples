data "aws_ami" "dev_server_ami" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]

  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["099720109477"] # Canonical

}

variable "pubkey" {
    type = string
}

resource "aws_key_pair" "dev_instance_key_pair" {
  key_name   = "dev_instance_key_pair"
  public_key = var.pubkey
}

resource "aws_instance" "dev_instance" {
  ami           = data.aws_ami.dev_server_ami.id
  instance_type = "t2.medium"
  subnet_id     = aws_subnet.dev_net_1.id
  associate_public_ip_address = true
  vpc_security_group_ids = [ aws_security_group.dev_net_sec_group.id ]
  iam_instance_profile = aws_iam_instance_profile.dev_instance_profile.name
  key_name = aws_key_pair.dev_instance_key_pair.key_name

  tags = {
    Name = "dev_instance"
  }
}
