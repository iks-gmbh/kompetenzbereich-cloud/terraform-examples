resource "aws_vpc" "dev_net" {
  cidr_block = "192.168.0.0/24"
  enable_dns_support = true
  enable_dns_hostnames = true
  tags = {
      Name = "dev_net"
  }
}

resource "aws_subnet" "dev_net_1" {
    vpc_id                      = aws_vpc.dev_net.id
    availability_zone           = "eu-west-1a"
    cidr_block                  = cidrsubnet(aws_vpc.dev_net.cidr_block, 4, 1)
    map_public_ip_on_launch     = true

    tags = {
        Name = "dev_net_1"
    }
}