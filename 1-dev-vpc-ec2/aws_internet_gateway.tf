resource "aws_internet_gateway" "dev_net_ig" {
  vpc_id = aws_vpc.dev_net.id

  tags = {
    Name = "dev_net_ig"
  }
}