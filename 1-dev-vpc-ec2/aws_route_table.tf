resource "aws_route_table" "dev_net_route_table" {
  vpc_id = aws_vpc.dev_net.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.dev_net_ig.id
  }
  tags = {
    Name = "dev_net_route_table"
  }
}

resource "aws_route_table_association" "dev_net_route_table_association_1" {
  subnet_id      = aws_subnet.dev_net_1.id
  route_table_id = aws_route_table.dev_net_route_table.id
}