resource "aws_security_group" "dev_net_sec_group" {
  vpc_id = aws_vpc.dev_net.id
  name        = "dev_net_sec_group"
  description = "Allow all traffic"

  egress {
    protocol   = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 1
    to_port = 65535
  }

  ingress {
    protocol   = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    from_port  = 80
    to_port    = 80
  }
  ingress {
    protocol   = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    from_port  = 8080
    to_port    = 8080
  }
  ingress {
    protocol   = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    from_port  = 443
    to_port    = 443
  }
  ingress {
    protocol   = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    from_port  = 8443
    to_port    = 8443
  }
  ingress {
    protocol   = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    from_port  = 22
    to_port    = 22
  }

  ingress {
    protocol   = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    from_port  = 18080
    to_port    = 18099
  }

  tags = {
    Name = "dev_net_sec_group"
  }
}