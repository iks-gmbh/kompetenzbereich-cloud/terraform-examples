provider "aws" {
  profile = "default"
  region  = "eu-west-1"
}

variable "name" {
  type = string
  default = "kafka-sandbox"
}