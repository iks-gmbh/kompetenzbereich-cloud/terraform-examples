data "aws_ami" "main" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]

  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["099720109477"] # Canonical

}

variable "pubkey" {
    type = string
}

resource "aws_key_pair" "main" {
  key_name   = var.name
  public_key = var.pubkey
}

resource "aws_iam_instance_profile" "main" {
  name = var.name
  role = aws_iam_role.main.name
}

resource "aws_instance" "main" {
  ami           = data.aws_ami.main.id
  instance_type = "t2.medium"
  subnet_id     = aws_subnet.main_1.id
  count = 3
  associate_public_ip_address = true
  vpc_security_group_ids = [ aws_security_group.main.id ]
  iam_instance_profile = aws_iam_instance_profile.main.name
  key_name = aws_key_pair.main.key_name

  tags = {
    Name = var.name
  }
}
