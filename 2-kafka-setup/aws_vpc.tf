resource "aws_vpc" "main" {
  cidr_block = "192.168.0.0/24"
  enable_dns_support = true
  enable_dns_hostnames = true
  tags = {
      Name = var.name
  }
}

resource "aws_subnet" "main_1" {
    vpc_id                      = aws_vpc.main.id
    availability_zone           = "eu-west-1a"
    cidr_block                  = cidrsubnet(aws_vpc.main.cidr_block, 4, 1)
    map_public_ip_on_launch     = true

    tags = {
        Name = var.name
    }
}