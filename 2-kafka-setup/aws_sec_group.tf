resource "aws_security_group" "main" {
  vpc_id = aws_vpc.main.id
  name        = var.name
  description = "Allow all traffic"

  egress {
    protocol   = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 1
    to_port = 65535
  }
  ingress {
    protocol   = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    from_port  = 9092
    to_port    = 9092
  }
  ingress {
    protocol   = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    from_port  = 2181
    to_port    = 2182
  }
  ingress {
    protocol   = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    from_port  = 2888
    to_port    = 2888
  }
  ingress {
    protocol   = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    from_port  = 3888
    to_port    = 3888
  }
  ingress {
    protocol   = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    from_port  = 22
    to_port    = 22
  }

  ingress {
    protocol   = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    from_port  = 18080
    to_port    = 18099
  }

  tags = {
    Name = var.name
  }
}